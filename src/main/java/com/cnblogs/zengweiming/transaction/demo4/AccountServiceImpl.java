package com.cnblogs.zengweiming.transaction.demo4;

import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT)
public class AccountServiceImpl implements AccountService {

	private AccountDao accountDao;

	public void setAccountDao(AccountDao accountDao) {
		this.accountDao = accountDao;
	}

	@Override
	public void transferSuccess(String from, String to, double money) {
		accountDao.removeMoney(from, money);
		accountDao.addMoney(to, money);
	}

	@Override
	public void transferFailure(String from, String to, double money) {
		accountDao.removeMoney(from, money);
		int i = 1 / 0;
		accountDao.addMoney(to, money);
	}

}
