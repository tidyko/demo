package com.cnblogs.zengweiming.transaction.demo2;

/**
 * 账号服务层接口定义
 * 
 * @author tidy
 *
 */
public interface AccountService {
	
	/**
	 * 转账成功
	 * 
	 * @param from	:转账账户
	 * @param to	:收款账户
	 * @param money	:金额
	 */
	public void transferSuccess(String from,String to,double money);
	
	/**
	 * 转账失败
	 * 
	 * @param from	:转账账户
	 * @param to	:收款账户
	 * @param money	:金额
	 */
	public void transferFailure(String from,String to,double money);
	

}
