package com.cnblogs.zengweiming.transaction.demo1;

/**
 * 账号持久化接口定义
 * 
 * @author tidy
 *
 */
public interface AccountDao {
	
	/**
	 * 给指定账户加钱
	 * 
	 * @param name	:账号名
	 * @param money	:金额
	 */
	public void addMoney(String name,double money);
	
	/**
	 * 给指定账号扣钱
	 * 
	 * @param name	:账号名
	 * @param money	:金额
	 */
	public void removeMoney(String name,double money);

}
