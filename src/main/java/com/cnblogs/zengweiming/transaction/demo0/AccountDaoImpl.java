package com.cnblogs.zengweiming.transaction.demo0;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class AccountDaoImpl extends JdbcDaoSupport implements AccountDao {

	@Override
	public void addMoney(String name, double money) {
		this.getJdbcTemplate().update("update account set money = money + ? where name = ? ",money,name);
	}

	@Override
	public void removeMoney(String name, double money) {
		this.getJdbcTemplate().update("update account set money = money - ? where name = ? ",money,name);
	}

}
