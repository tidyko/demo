package com.cnblogs.zengweiming.transaction.demo;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.cnblogs.zengweiming.transaction.demo3.AccountService;

/**
 * 声明式事务:使用AspectJXML
 * 
 * @author tidy
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/transaction/applicationContext3.xml")
public class Demo3 {

	private static final Logger logger = LoggerFactory.getLogger(Demo3.class);

	@Autowired
	private AccountService accountService;

	/**
	 * 转账失败
	 */
	@Test
//	@Ignore
	public void testTransferFailure() {
		try {
			accountService.transferFailure("A", "B", 200d);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	/**
	 * 转账成功
	 */
	@Test
	@Ignore
	public void testTransferSuccess() {
		accountService.transferSuccess("A", "B", 200d);
	}

}
