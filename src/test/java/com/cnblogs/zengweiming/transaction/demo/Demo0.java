package com.cnblogs.zengweiming.transaction.demo;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.cnblogs.zengweiming.transaction.demo0.AccountService;

/**
 * 没使用事务，
 * 会导致转账失败的情况下，数据丢失一致性。
 * 
 * @author tidy
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/transaction/applicationContext0.xml")
public class Demo0 {

	private static final Logger logger = LoggerFactory.getLogger(Demo0.class);

	@Autowired
	private AccountService accountService;

	/**
	 * 转账失败
	 */
	@Test
//	@Ignore
	public void testTransferFailure() {
		try {
			accountService.transferFailure("A", "B", 200d);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	/**
	 * 转账成功
	 */
	@Test
	@Ignore
	public void testTransferSuccess() {
		accountService.transferSuccess("A", "B", 200d);
	}

}
